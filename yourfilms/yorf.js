/**
 * Created by sl0 on 26.06.2017.
 */
console.log("Начало скрипта");

var raw = document.querySelector("#wrapper h2");
var parentDiv = raw.parentNode;

var film = raw.innerHTML.match( /.+/ )[0];
film = 'https://www.kinopoisk.ru/s/type/film/list/1/find/'+encodeURIComponent(film);
console.log("фильм: "+film);

function getElement(url, selector, c) {
	request(new XMLHttpRequest());
	
	function request(xhr) {
		xhr.open('GET', url, true);
		
		xhr.setRequestHeader('Content-Type', 'text/plain');
		xhr.send();
		 xhr.onreadystatechange = function() {
			if(xhr.readyState === 4) {
				if(xhr.status === 200) {
					html = document.createElement('div');
					html.innerHTML = xhr.responseText;
					c(html.querySelector(selector));
				}
			}
		}
	}
}

getElement(film, "div.search_results div.element div.info p.name a", function(element) {
	var url = 'https://www.kinopoisk.ru'+element.getAttribute('data-url');
	element.style.fontSize = '24px';
	// useUrl(element.href);
	element.href = url;
	parentDiv.insertBefore(element, raw.nextSibling);
	raw.appendChild(element);
	useUrl(url);
});

function useUrl(urla) {
	console.log("урла useUrl: "+urla);
	getElement(urla, "div.brand_words.film-synopsys", function(element) {
		console.log("title: "+element.innerHTML);
		var parentElem = document.querySelector("#wrapper .row.delimited .span14");
		var child = document.querySelector("#wrapper .row.delimited .span14 dl");
		if (!parentElem) {
			parentElem = document.querySelector("#wrapper .row.delimited .span12");
			child = document.querySelector("#wrapper .row.delimited .span12 dl");
		}
		// var br = document.createElement("br");
		// element.appendChild(br);
		// element.style.display = 'inline-block';
		element.style.textIndent = "2em";
		element.style.paddingBottom = "20px";
		console.log("Описание загружено!");
		parentElem.insertBefore(element, child);
	});
}