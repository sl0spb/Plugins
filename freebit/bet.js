const startBet = [0.00000001, 0.00000002, 0.00000003, 0.00000004, 0.00000005, 0.00000006, 0.00000007, 0.00000008, 0.00000009, 0.0000001, 0.00000011, 0.00000012, 0.00000015, 0.0000002],
	startPayout = [4750, 79.17, 39.92, 26.69, 20.04, 16.05, 13.38, 11.47, 10.04, 8.93, 8.04, 7.31, 6.7, 6.18],
	stopPercentage = 0.001,
	maxWait = 505;
const $loButton = document.getElementById('double_your_btc_bet_lo_button'),
	$hiButton = document.getElementById('double_your_btc_bet_hi_button');

function multiply(bet, payout) {
	const currentEl = document.getElementById('double_your_btc_stake');
	var currentPay = document.getElementById('double_your_btc_payout_multiplier');
	// const currentPay = document.getElementById('double_your_btc_win_chance');
	// console.log("OldBet: "+current);
	currentEl.value = (bet).toFixed(8);
	currentPay.value = (95 / payout).toFixed(2);
	// console.log("NewBet: "+current);
}

function getRandomWait() {
	const wait = Math.floor(Math.random() * maxWait) + 500;
	console.log('Waiting for ' + wait + 'ms before next bet.');
	return wait;
}

function startGame() {
	console.log('Game started!');
	reset();
	$loButton.click();
}
function stopGame() {
	console.log('Game will stop soon! Let me finish.');
	stopped = true;
}

function reset() {
	const currentEl = document.getElementById('double_your_btc_stake');
	currentEl.value = startBet;
}


function iHaveEnoughMoni() {
	const balance = parseFloat(document.getElementById('balance').innerHTML);
	const current = document.getElementById('double_your_btc_stake').value;
	return ((balance) * 2 / 100) * (current * 2) > stopPercentage / 100;
}

function oldBalance() {
	const balance = parseFloat(document.getElementById('balance').innerHTML);
	return balance;
}

function stopBeforeRedirect() {
	const minutes = parseInt($('title').text());
	if (minutes < stopBefore) {
		console.log('Approaching redirect! Stop the game so we don\'t get redirected while loosing.');
		stopGame();
		return true;
	}
	return false;
}


let i = 0.02;
const j = 3;
const old = oldBalance();
const win = document.getElementById('double_your_btc_bet_win');
win.style.display = 'none';
console.log('Баланс в САМОМ начале: ' + old);
(function () {
	if (i < 4.26) {
		if (win.style.display === 'block') {
			const profit1 = parseFloat(oldBalance() - old);
			console.log('Профит одного раунда: ' + profit1.toFixed(8));
			throw new Error("stop");
		}
		const currentBet = 0.00000001,
	currentPayout = i;
		multiply(currentBet, currentPayout);
		const pressWhat = Math.floor(Math.random() * (2));
		if (pressWhat === 0) {
			$loButton.click();
			// console.log('На;али LO');
		} else {
			$hiButton.click();
			// console.log('Нажали HI!');
		}
		i+= 0.01;
		setTimeout(arguments.callee, getRandomWait());
	} else {
		console.log('Просрано 426 циклов. Минус 113 сатоши');
}
})();


// $('#double_your_btc_bet_lose').unbind();
// $('#double_your_btc_bet_win').unbind();
// $('#double_your_btc_bet_lose').bind("DOMSubtreeModified", function (event) {
// 	if ($(event.currentTarget).is(':contains("lose")')) {
// 		console.log('You LOST! Multiplying your bet and betting again.');
// 		multiply();
// 		setTimeout(function () {
// 			$loButton.trigger('click');
// 		}, getRandomWait());
// 	}
// });
// $('#double_your_btc_bet_win').bind("DOMSubtreeModified", function (event) {
// 	if ($(event.currentTarget).is(':contains("win")')) {
// 		if (stopBeforeRedirect()) {
// 			return;
// 		}
// 		if (iHaveEnoughMoni()) {
// 			console.log('You WON! But don\'t be greedy. Restarting!');
// 			reset();
// 			if (stopped) {
// 				stopped = false;
// 				return false;
// 			}
// 		}
// 		else {
// 			console.log('You WON! Betting again');
// 		}
// 		setTimeout(function () {
// 			$loButton.trigger('click');
// 		}, getRandomWait());
// 	}
// });
// startGame()
// oneRound();

// (function () {
// 	if (j < 1) {
// 		console.log('Новый баланс: ' + oldBalance());
// 		var profit = parseFloat(oldBalance() - old);
// 		console.log('По прошествии 10 раундов профит: ' + profit.toFixed(8));
// 		return;
// 	}
// 	if (i < 14) {
// 		if (win.style.display === 'block') {
// 			var profit1 = parseFloat(oldBalance() - old);
// 			console.log('Профит одного раунда: ' + profit1.toFixed(8));
// 			i = 0;
// 			j--;
// 			win.style.display = 'none';
// 		}
// 		var currentBet = startBet[i],
// 			currentPayout = startPayout[i];
// 		multiply(currentBet, currentPayout);
// 		var pressWhat = Math.floor(Math.random() * (2));
// 		if (pressWhat === 0) {
// 			$loButton.click();
// 			// console.log('На;али LO');
// 		} else {
// 			$hiButton.click();
// 			// console.log('Нажали HI!');
// 		}
// 		i++;
// 		setTimeout(arguments.callee, getRandomWait());
// 	} else {
// 		console.log('Просрано 14 циклов. Минус 113 сатоши');
// 		j--;
// 		setTimeout(arguments.callee, getRandomWait());
// 	}
// })();